LibInputSynth
=============

Synthesize keyboard and mouse input on X11 and Wayland with various backends.

## Requirements

Backends (all optional):

- xdotool
- libxi

## Run the tests

Run all tests
```
$ ninja -C build test
```

## Usage

- Include the main header `#include <inputsynth.h>`
- Link to the main .so file `libinputsynth-0.8.so`

This library contains a pkgconfig file for `libinputsynth`.

See the examples for creating an instance and synthesizing input.

### Usage in development

libinputsynth loads backend libraries with the system's library search method.

If the build directory of the library is in ./build,
running an example such that it loads a backend from the build directory
rather than e.g. `/usr/lib/`, use `LD_LIBRARY_PATH` like this

    LD_LIBRAY_PATH=$PWD/build/src ./build/examples/inputsynth -b xdo

### Input relative to the entire desktop:

```c
// the chosen backend will be dynamically loaded
InputSynth *input = INPUT_SYNTH (input_synth_new (InputsynthBackend_XDO));

// the rest of the code is the same for all backends

input_synth_move_cursor (input, 100, 100);

/* Right click (button 3) press and release */
input_synth_click (input, 100, 100, 3, TRUE);
usleep (5000);
input_synth_click (input, 100, 100, 3, FALSE);

input_synth_char (input, 'h');
input_synth_char (input, 'i');

g_object_unref (input);
```

## Install

```
sudo ninja -C build install
```

## Running without installing

Set the environment variable `LIBINPUTSYNTH_PLUGINDIR` to the path where
`libinputsynth_*.so` was built, for example:

```bash
LIBINPUTSYNTH_PLUGINDIR=~/libinputsynth/build/src build/examples/inputsynth -b xdo
```

## Code of Conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

* First-line project contacts:
  * Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
  * Christoph Haag <christoph.haag@collabora.com>
* freedesktop.org contacts: see most recent list at <https://www.freedesktop.org/wiki/CodeOfConduct/>

